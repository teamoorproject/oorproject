import logo from './logo.svg';
import './App.css';
import React from 'react';
import { Route, Switch } from 'react-router';
import Header from './components/General/Header';
import Footer from './components/General/Footer';

function App() {
  return (
  /*  <Switch>
    <Route path="/stella" exact component={Stella} />
   </Switch> */
   <div>
    <Header />
    <Footer />
   </div>


  );
}

export default App;
